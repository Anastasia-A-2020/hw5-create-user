const user = createNewUser();
const login = user.getLogin();
console.log(login);

function createNewUser() {
  const firstName = prompt("Enter your name");
  const lastName = prompt("Enter your lastname");

  const newUser = {
    _firstName: "",
    _lastName: "",

    set firstName(value) {
      this._firstName = value;
    },

    set lastName(value) {
      this._lastName = value;
    },
    
    getLogin() {
      return `${this._firstName[0].toLowerCase()}${this._lastName.toLocaleLowerCase()}`;
    },
  }

  newUser.firstName = firstName;
  newUser.lastName = lastName;
 
  return newUser;
}